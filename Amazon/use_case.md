**Znaleźć produkcję i obejrzeć ją aktywując okres próbny usługi Amazon Prime**

**Legenda**

U- użytkownik

S- system

**Scenariusz:**

1.	U najeżdża kursorem na zakładkę *Account & Lists* i wybiera opcję *Your Prime Video*

2.	S wyświetla stronę z dostępnymi produkcjami na platformie *Amazon Prime*

3.	U wyszukuje interesującą go produkcję i klika w jej ikonę

4.	S wyświetla stronę szczegółową produkcji z opcją obejrzenia jej w okresie próbnym *Prime*

5.	U wybiera opcję rozpoczęcia okresu próbnego

6.	S wyświetla okno logowania użytkownika

7.	U wprowadza dane i loguje się do konta użytkownika

8.	S wyświetla okno z polem do wprowadzenia danych karty płatniczej
	
9.	U uzupełnia wymagane pola 
	
10.	S wyświetla potwierdzenie transakcji i wyświetla informację o rozpoczęciu okresu próbnego

11.	U przechodzi do strony szczegółowej produkcji
	
12.	S wyświetla stronę szczegółową produkcji
	
13.	U wybiera opcję oglądania produkcji

14.	S wyświetla produkcję

**Alternatywne warianty**

* **2a**. Jeżeli U jest poza USA, to S wyświetla informacje o możliwości braku dostępności niektórych swoich produkcji w innych krajach. Przejście do kroku 3

* **3a**. U wyszukuje i klika w ikonę produkcji spoza *Amazon Original*s S wyświetla stronę produkcji, bez opcji jej obejrzenia w okresie próbnym. Koniec scenariusza

* **4a**. U jest zalogowany i ma wykupioną subskrypcję *Amazon Prime* lub aktywował miesiąc próbny. Przejście od kroku 13

* **9a**. U nie posiada karty płatniczej. Koniec scenariusza


**Parametry**

Wyszukiwanie po kategoriach: *Coming soon*, *Movies*, *Tv shows*, *Included with Prime*, *Amazon Originals*, *Sports*, *New Releases*, *4K UHD*, *Featured deals*, *Shorts*

