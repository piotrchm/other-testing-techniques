**Znaleźć serial w bazie FIlmweb o określonych parametrach do obejrzenia na Netflix**

**Legenda**

U- użytkownik

S- system

**Scenariusz:**

1. U wchodzi w zakładkę Seriale/Baza Seriali

2. S wyświetla Bazę Filmwebu

3. U wybiera i zaznacza filtry dla seriali w Bazie Filmweb

4. S wyświetla listę wyników zgodnych z wybranymi filtrami

5. U sortuje wyświetlone wyniki

6. S wyświetla listę wyników w kolejności wybranego sortowania

7. U szuka na liście wyników pozycji z adnotacją *Oglądaj na NETFLIX* i wchodzi na jego stronę

8. S wyświetla stronę wybranego serialu

**Alternatywne warianty**


* **3a**. U wybiera filtry wymagające zalogowania. S wyświetla panel logowania. Jeżeli U się zaloguje- powrót do kroku 3 (możliwość zaznaczenia filtrów dla U zalogowanych). Jeżeli U nie zaloguje się- powrót do kroku 3 (nie może zaznaczyć filtrów wymagających zalogowania)

* **4a**. Serialu nie znaleziono. S wyświetla komunikat o błędzie. Koniec scenariusza

* **7a**. U klika adnotację *Oglądaj na NETFLIX*. S otwiera serial na platformie NETFLIX. Koniec scenariusza

* **7b**. U nie znajduje serialu z adnotacją *Oglądaj na NETFLIX*. Koniec scenariusza.

**Parametry**

filtry wyszukiwania: gatunek, kraj produkcji, lata produkcji, liczba głosów, pokaż bez oceny, ukryj obejrzane, ukryj „nie interesuje mnie”

sortowanie: według: ocen, liczby ocen, roku produkcji, chcę zobaczyć, popularności